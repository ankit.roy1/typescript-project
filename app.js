var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var Person = /** @class */ (function () {
    function Person(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    Person.quickSort = function (personArray, field, order) {
        var tempArray = __spreadArray([], personArray, true);
        if (tempArray.length <= 1) {
            return tempArray;
        }
        var pivotItem = tempArray[tempArray.length - 1];
        var pivot = pivotItem[field];
        var leftArray = [];
        var rightArray = [];
        for (var _i = 0, _a = tempArray.slice(0, tempArray.length - 1); _i < _a.length; _i++) {
            var element = _a[_i];
            var ele = element;
            if (order === "asc") {
                ele[field] < pivot ? leftArray.push(ele) : rightArray.push(ele);
            }
            else {
                ele[field] < pivot ? rightArray.push(ele) : leftArray.push(ele);
            }
        }
        if (leftArray.length > 0 && rightArray.length > 0) {
            return __spreadArray(__spreadArray(__spreadArray([], this.quickSort(leftArray, field, order), true), [
                pivotItem
            ], false), this.quickSort(rightArray, field, order), true);
        }
        else if (leftArray.length > 0) {
            return __spreadArray(__spreadArray([], this.quickSort(leftArray, field, order), true), [pivotItem], false);
        }
        else {
            return __spreadArray([pivotItem], this.quickSort(rightArray, field, order), true);
        }
    };
    return Person;
}());
var person1 = new Person("Ankit", 13, 25000, "M");
var person2 = new Person("Sitesh", 21, 42000, "M");
var person3 = new Person("Bittu", 13, 39000, "M");
var person4 = new Person("Shubham", 42, 12000, "M");
var person5 = new Person("Tony", 35, 55000, "M");
var personArray = [person1, person2, person3, person4, person5];
console.log(Person.quickSort(personArray, "salary", "asc"));
