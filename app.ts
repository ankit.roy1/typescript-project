class Person {

    name   : string;
    age    : number;
    salary : number;
    sex    : string; 

    constructor(name: string, age: number, salary: number,sex: string) {
        this.name   = name;
        this.age    = age;
        this.salary = salary;
        this.sex    = sex;
    }

    public static quickSort(personArray:Person[],field:string,order:string):Person[] {
        let tempArray:Person[]=[...personArray]

        if(tempArray.length <= 1){
            return tempArray;
        }

        let pivotItem : any = tempArray[tempArray.length - 1];
        let pivot : number  = pivotItem[field];
    
        let leftArray  : Person[] = [];
        let rightArray : Person[] = [];
    
        for (let element of tempArray.slice(0, tempArray.length - 1)) {
            let ele:any=element;
            
            if (order === "asc") {
            ele[field] < pivot ? leftArray.push(ele) : rightArray.push(ele);
          } else {
            ele[field] < pivot ? rightArray.push(ele) : leftArray.push(ele);
          }
        }
    
        if (leftArray.length > 0 && rightArray.length > 0) {
          return [
            ...this.quickSort(leftArray, field, order),
            pivotItem,
            ...this.quickSort(rightArray, field, order)
          ];
        } else if (leftArray.length > 0) {
          return [...this.quickSort(leftArray, field, order), pivotItem];
        } else {
          return [pivotItem, ...this.quickSort(rightArray, field, order)];
        }
  
    }
}

let person1 = new Person("Ankit", 13, 25000, "M");
let person2 = new Person("Sitesh", 21, 42000, "M");
let person3 = new Person("Bittu", 13, 39000, "M");
let person4 = new Person("Shubham", 42, 12000, "M");
let person5 = new Person("Tony", 35, 55000, "M");
const personArray = [person1, person2, person3, person4, person5];

console.log(Person.quickSort(personArray, "salary", "asc"));
